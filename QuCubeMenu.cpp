#include "QuCubeMenu.h"



QuCubeMenu::QuCubeMenu(float aScale):mOptions(6),mCubeScale(aScale)
{
	//temp
	mInvalidImage = G_IMAGE_MANAGER.getFlyImage("images/CAT.png");

	mFace.setCount(4);
	mFace.addColor(0,ARRAYN<float,12>::conv2<4,4>(CUBE_MENU_FACE_COLOR));
	mFace.setKey(0);
	mFace.loadVertices( ARRAYN<float,12>::conv2<4,3>(CUBE_MENU_FACE_COORDS) );
	mFace.loadNormals( ARRAYN<float,12>::conv2<4,3>(CUBE_MENU_FACE_NORMALS) );
}
void QuCubeMenu::draw()
{	
	//TODO delete
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	QuMaterialPrefabs::getMaterial("RED").enable();
	

	static int facemap[4] = { 0, 2, 1, 3 };
	glPushMatrix();
	glScalef(mCubeScale,mCubeScale,mCubeScale);

	//draw faces 0-3
	for(int i = 0; i < 4; i++)
	{
		setDrawObject(facemap[i]);
		drawFace();
		glRotatef(90,0,0,1);
	}

	//now draw faces 4 and 5
	glRotatef(90,0,1,0);
	setDrawObject(4);
	drawFace();
	glRotatef(-180,0,1,0);
	setDrawObject(5);
	drawFace();

	mFace.disable();
	glPopMatrix();
}
void QuCubeMenu::setInvalidImage(QuStupidPointer<QuBaseImage> aImg)
{ mInvalidImage = aImg; }
void QuCubeMenu::addOption(QuStupidPointer<QuCubeMenuOption> aOption, int index)
{
	if(index == -1)
	{
		int index = 0;
		while(index < 6 && !mOptions[index].isNull())
			index++;
	}
	mOptions[index] = aOption;
}

void QuCubeMenu::addNewMenuOption(QuStupidPointer<QuCubeMenu> menu, QuStupidPointer<QuBaseImage> aImg, int index)
{
	//TODO
	quAssert(false);
}

void QuCubeMenu::setDrawObject(int index)
{
	if(mOptions[index].isNull())
	{
		if(!mInvalidImage.isNull())
			mFace.loadImage(mInvalidImage);
	}
	else
	{
		if(!mOptions[index]->getImage().isNull())
			mFace.loadImage(mOptions[index]->getImage());
	}
}

void QuCubeMenuManager::update()
{
	//TODO
	glClearColor(1,1,1,1);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	//glClear(GL_DEPTH_BUFFER_BIT);
	draw();
}
void QuCubeMenuManager::draw()
{
	QuVector3<float> rot = G_GAME_GLOBAL.getAccel();
	mCam.setRotation(G_DEVICE_ROTATION,0);
	mCam.setTiltTarget(rot.x,rot.y);
	mCam.update();
	mCam.setScene();
	mCam.setRotozoomModelMatrix();
	//light after rotation means light always stays at the top face
	mLights.enable();
	QuCoordinateSystemDrawer().draw();
	QuCubeMenu().draw();
	mLights.disable();


	//TODO
}

QuCubeMenuManager::QuCubeMenuManager():mCam(G_SCREEN_WIDTH)
{
	mLights.setLight(GL_POSITION,ARRAYN<float,3>(CUBE_MENU_LIGHT_POSITION));
}