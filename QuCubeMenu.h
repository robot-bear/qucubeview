#pragma once
#include "QuGameManager.h"
#include "QuCubeMenuConstants.h"
#include "QuCubeView.h"
#include "QuMaterials.h"
#include <set>


//putting this here for now as a test. we'll see how it goes, maybe all those stupid poniters is a bad idea
//T - functor type
//ARG - unary functor argument type
template<typename T, typename ARG> 
class QuCubeSingleEventManager
{
	std::set<QuStupidPointer<T> > mEventHandlerList;
public:
	void addEventListener(QuStupidPointer<T> aListener)
	{
		mEventHandlerList.insert(aListener);
	}
	bool removeEventListener(QuStupidPointer<T> aListener)
	{
		return mEventHandlerList.erase(aListener) == 1;
	}
	void triggerEvent( ARG a1 ) 
	{
		for(typename std::set<QuStupidPointer<T> >::iterator it = mEventHandlerList.begin(); it != mEventHandlerList.end(); it++)
		{
			it->getReference()(a1);
		}
	}
};


class QuCubeMenu;
//TODO switch this over to a generalized event manager
class QuCubeMenuOption
{
	QuStupidPointer<QuBaseImage> mImage;
public:
	struct ClickEventArgStruct
	{
		ClickEventArgStruct( QuCubeMenuOption & aOption, QuCubeMenu & aMenu):
			mOption(aOption),mMenu(aMenu){}
		QuCubeMenuOption & mOption;
		QuCubeMenu & mMenu;
	};
	class ClickEvent : public QuUnaryFunctor<ClickEventArgStruct>
	{
	public:
		virtual void operator()(ClickEventArgStruct a1) = 0;
	};
private:
	QuCubeSingleEventManager<ClickEvent,ClickEventArgStruct> mClickEventMan;
	QuStupidPointer<QuCubeMenu> mParentMenu;
public:
	QuCubeMenuOption(QuStupidPointer<QuBaseImage> aImage = QuStupidPointer<QuBaseImage>(NULL)):mImage(aImage)
	{}
	void setImage(QuStupidPointer<QuBaseImage> aImage){ mImage = aImage; }
	void addClickEvent( QuStupidPointer<ClickEvent> evt )
	{mClickEventMan.addEventListener(evt);}
	void removeClickEvent( QuStupidPointer<ClickEvent> evt )
	{mClickEventMan.removeEventListener(evt);}
	void clicked()
	{mClickEventMan.triggerEvent(ClickEventArgStruct(*this,mParentMenu.getReference()));}
	QuStupidPointer<QuBaseImage> getImage(){ return mImage; }
};

class QuCubeMenu
{
	QuStupidPointer<QuBaseImage> mInvalidImage;
	QuColorableDrawObject<int> mFace;
	std::vector<QuStupidPointer<QuCubeMenuOption> > mOptions;
	float mCubeScale;
public:
	class QuSubMenuEvent : QuCubeMenuOption::ClickEvent
	{
	public:
		QuStupidPointer<QuCubeMenu> mNextMenu;
		QuSubMenuEvent(QuStupidPointer<QuCubeMenu> aNextMenu):mNextMenu(aNextMenu){}
		void operator()(QuCubeMenuOption::ClickEventArgStruct a1)
		{

		}
	};
	QuCubeMenu(float aScale = CUBE_MENU_DEFAULT_SCALE);
	void setInvalidImage(QuStupidPointer<QuBaseImage> aImg);
	void addOption(QuStupidPointer<QuCubeMenuOption> aOption, int index = -1);
	void addNewMenuOption(QuStupidPointer<QuCubeMenu> menu, QuStupidPointer<QuBaseImage> aImg, int index = -1);	//easy way to add a sub menu
	void draw();
private:
	void drawFace(){ mFace.enable(); mFace.draw(); }
	void setDrawObject(int index);
};


class QuCubeMenuManager : public QuBaseManager
{
	QuCubeCamera mCam;
	QuLights mLights;
public:
	QuCubeMenuManager();
	virtual void update();
	virtual void singleUp(int button, QuScreenCoord crd){};
	virtual void singleMotion(QuScreenCoord crd){};
	//TODO
	void setNewMenu(QuStupidPointer<QuCubeMenu> aMenu){}
private:
	void draw();
};

