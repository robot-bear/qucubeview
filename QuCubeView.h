#pragma once
#include "QuCMath.h"
#include "QuMath.h"
#include "QuUtils.h"
#include "QuCamera.h"
#include "QuCubeViewConstants.h"

class QuDynamicRotation
{
	float * mTarRot;
	QuQuaternion mTarQuat;
	QuQuaternion mPrevQuat;
	QuQuaternion mCurQuat;

	int mMaxTime;
	QuTimer mTimer;
public:
	const QuQuaternion & getCurQuat() const
	{
		return mCurQuat;
	}
	void reset()
	{
		mMaxTime = 20;

		//load the starting transform
		mTarRot = convertMatrixType<int,float,9>(DIRECTION_ENUM_TO_R_MATRIX[0]);
		multiplyTarByMatrix(MATRIX_R_IDENTITY);
		mTimer.expire();
		mCurQuat = QuQuaternion(1,0,0,0);
		mTarQuat = QuQuaternion(1,0,0,0);
		mPrevQuat = QuQuaternion(1,0,0,0);
	}
	QuDynamicRotation():mCurQuat(1,0,0,0),mTarQuat(1,0,0,0),mPrevQuat(1,0,0,0)
	{
		reset();
	}
	~QuDynamicRotation()
	{
		delete [] mTarRot;
	}

	//this should really be a const reference but I'm not going to bother
	QuTimer & getTimer()
	{
		return mTimer;
	}

	void setTargetRotation(float * aTar)
	{
		mTimer = QuTimer(0,mMaxTime);
		memcpy(mTarRot,aTar,sizeof(float)*9);

		float * vals = convertMatrixToQuaternion(mTarRot);
		mTarQuat = QuQuaternion(vals[3],vals[0],vals[1],vals[2]);
		delete [] vals;

		mPrevQuat = mCurQuat;
	}

	void multiplyTarByMatrix(const int * m)
	{
		float * converted = convertMatrixType<int,float,9>(m);
		float * product = multiplyMatrices3<float>(converted,mTarRot);
		setTargetRotation(product);
		delete [] converted;
		delete [] product;
	}
	

	void update()
	{
		mCurQuat = QuQuaternion::slerp(mPrevQuat,mTarQuat,mTimer.getSquareRoot());
		//mCurQuat = QuQuaternion::slerp(mPrevQuat,mTarQuat,mTimer.getLinear());
		mTimer.update();
	}
	void applyQuaternionRotation(QuQuaternion r)
	{
		float * m3 = r.convertToM3();
		float * m = convertM3ToM4(m3);
		glMultMatrixf(m);
		delete [] m;
		delete [] m3;
	}
	void applyTransform()
	{
		applyQuaternionRotation(mCurQuat);
	}
};

class QuCubeOrient
{
private:
	unsigned mOrient[6];
public:
	QuCubeOrient(unsigned short aStart = 0)
	{
		//TODO I think this actually needs to start at 3 to work
		for(unsigned i = 0; i < 6; i++)
			mOrient[i] = ROTATION_TO_ORIENTATION[aStart][i];
	}
	unsigned getCurrentFace() const { return mOrient[0]; }
	unsigned getOppositeFace() const { return mOrient[1]; }
	unsigned getFaceInDirection(QuTurnDirections aDir) const { return mOrient[aDir]; }
	QuTurnDirections getDirectionOfFace(unsigned aFace) const
	{
		for(int i = 0; i < 6; i++)
		{
			if(i == 1)
				continue;
			if(aFace == mOrient[i])
				return (QuTurnDirections)i;
		}
		return INVALID_DIRECTION;
	}
	
	bool applyRotation(QuTurnDirections aDir)
	{
		if(aDir == INVALID_DIRECTION)
			return false;
		unsigned oldOrient [6];
		memcpy(&oldOrient,&mOrient,sizeof(unsigned)*6);
		for(unsigned i = 0; i < 6; i++)
			mOrient[i] = oldOrient[ROTATION_TO_ORIENTATION[(unsigned)aDir][i]];
		return true;
	}
};

class QuCubeCamera : public QuBasicCamera
{
	float mBaseScale;
	QuDynamicRotation mRot;
	QuCubeOrient mOrient;
	QuSho mPhi, mTheta;
public:
	QuCubeCamera(int aVpWidth,int aVpHeight = -1,float aDistance = DEFAULT_CAMERA_DISTANCE,float aBaseScale = DEFAULT_CUBE_SCALE);
	~QuCubeCamera(){}
	
	void reset(){mRot.reset();}
	const QuCubeOrient & getOrientation(){ return mOrient; }
	QuQuaternion getInverseRotation();
	bool rotateInDirection(QuTurnDirections aDir);
	void addImpulse(float aX, float aY);
	void setTiltTarget(QuQuaternion q);
	void adjustTiltTarget(float aX, float aY);
	void setTiltTarget(float aX, float aY, float aZ = -1);
	//TODO
	void setShake(float aIntensity, int aTime){	quAssert(false); }
	void setHeightAndDistanceFromRotation();
	void update();
	void setRotozoomModelMatrix();
};