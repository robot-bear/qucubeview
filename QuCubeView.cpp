#include "QuCubeView.h"




QuCubeCamera::QuCubeCamera(int aVpWidth,int aVpHeight,float aDistance,float aBaseScale)
	:QuBasicCamera(aVpWidth,aVpHeight,aDistance),mBaseScale(aBaseScale)
{
	setHeightAndDistanceFromRotation();
}

QuQuaternion QuCubeCamera::getInverseRotation()
{
	return 1 / (
		mRot.getCurQuat() *
		QuQuaternion::fromAngleAxis(-ANGLES_TO_RADIANS*mTheta.getValue(),-1,0,0) *
		QuQuaternion::fromAngleAxis(-ANGLES_TO_RADIANS*mPhi.getValue(),0,1,0) );
		
}
bool QuCubeCamera::rotateInDirection(QuTurnDirections aDir)
{
	if(aDir == IDENTITY_DIRECTION)
		return false;
	mRot.multiplyTarByMatrix(DIRECTION_ENUM_TO_R_MATRIX[(unsigned)aDir]);
	mOrient.applyRotation(aDir);
	return true;
}
void QuCubeCamera::addImpulse(float aX, float aY)
{
	mTheta.addV(aY/40);
	mPhi.addV(-aX/40);
}
void QuCubeCamera::setTiltTarget(QuQuaternion q)
{
	float h,a,b;
	q.toEulerAngles(h,a,b);
	mPhi.setXNot(h*RADIANS_TO_ANGLES);
	mTheta.setXNot(a*RADIANS_TO_ANGLES);
}
void QuCubeCamera::adjustTiltTarget(float aX, float aY)
{
	mPhi.setXNot( mPhi.getXNot() + aX);
	mTheta.setXNot( mTheta.getXNot() + aY);
}
void QuCubeCamera::setTiltTarget(float aX, float aY, float aZ)
{
	if(aZ > 0)
		return;
	float lambda = pow((quClamp<float>(1-quAbs<float>(aZ),0,1)),3);
	float mag = sqrt(aX*aX + aY*aY);
	mag = lambda*mag + (1-lambda);
	if(mag != 0)
	{
		aX /= mag;
		aY /= mag;
	}
	float s = -140;
    //mPhi.setXNot(aY*aY*aY*s);
    //mTheta.setXNot(aX*aX*aX*s);
	//mPhi.setXNot(quAbs<float>(aY)*aY*s);
	//mTheta.setXNot(quAbs<float>(aX)*aX*s);
	mPhi.setXNot(pow(quAbs<float>(aY),1.5f)*aY*s);
	mTheta.setXNot(pow(quAbs<float>(aX),1.5f)*aX*s);
}
void QuCubeCamera::setHeightAndDistanceFromRotation()
{
	float hScale = cos(mRot.getTimer().getParabola(QUARTER_PI,0))*ONE_OVER_COS_QUARTER_PI;
	hScale = (hScale-1)*0.4 +1;

	float maxRot = quMax<float>(quAbs<float>(mPhi.getValue()),quAbs<float>(mTheta.getValue()));
	float cRot = (90-quClamp<float>(maxRot,0,45));
	float hScale2 = sin(cRot*ANGLES_TO_RADIANS)*ONE_OVER_COS_QUARTER_PI;
	hScale2 = (hScale2-1)*0.7 +1;

	mScale = mBaseScale * sqrt(hScale2*hScale2 + hScale*hScale)*ROOT_TWO_OVER_TWO;

	//TODO calculate distance
	//mCameraDistance = mBaseCameraDistance*(mRot.getTimer().getUpsidedownParabola()*100+1);
}
void QuCubeCamera::update()
{
	mPhi.updateWithFakeCriticalDamping();
	mTheta.updateWithFakeCriticalDamping();
	mRot.update();
	setHeightAndDistanceFromRotation();
}

void QuCubeCamera::setRotozoomModelMatrix()
{
	glRotatef(mPhi.getValue(),0,1,0);
	glRotatef(mTheta.getValue(),-1,0,0);
	mRot.applyTransform();
	quScale(mScale);
}