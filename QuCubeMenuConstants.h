#pragma once

//this should be face 0 I hope
static GLfloat CUBE_MENU_FACE_COORDS[4][3] = {0.5,-0.5,-0.5, 0.5,-0.5,0.5,  0.5,0.5,-0.5,  0.5,0.5,0.5};	
//static GLfloat CUBE_MENU_FACE_COORDS[4][3] = {0,-0.5,-0.5, 0,-0.5,0.5, 0,0.5,-0.5, 0,0.5,0.5};	
static GLfloat CUBE_MENU_FACE_NORMALS[4][3] = {1,0,0,   1,0,0, 1,0,0, 1,0,0};
static GLfloat CUBE_MENU_FACE_COLOR[4][4] = {1,1,1,1,   1,1,1,1, 1,1,1,1, 1,1,1,1};

static GLfloat CUBE_MENU_LIGHT_POSITION[3] = {10,0,0};

static float CUBE_MENU_DEFAULT_SCALE = 0.5f;