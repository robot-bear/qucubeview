#pragma once
#include "QuUtils.h"
#include "QuCMath.h"

static float DEFAULT_CUBE_SCALE = 1;	//TODO compute me as a function of cube width and height

//these are array indices used on mOrient
enum QuTurnDirections
{
	IDENTITY_DIRECTION = 0,
	UP = 4,
	DOWN = 5,
	LEFT = 2,
	RIGHT = 3,
	INVALID_DIRECTION = 6
};

static const int FACE_TO_POSSIBLE_TURNS[6][4] = 
{
	2, 3, 4, 5, 2, 3, 4, 5,
	0, 1, 4, 5, 0, 1, 4, 5,
	0, 1, 2, 3, 0, 1, 2, 3
};

//this is used by QuDrawing.h to rotate face 0 to another face
static const int FACE_TO_AXIS_ANGLE[6][2] =
{
	2,0,
	2,180,
	1,-90,
	1,90,
	2,-90,
	2,90
};

static int VECTOR_INDEX_TO_AXIS[3][3] =
{
	1,0,0,
	0,1,0,
	0,0,1
};

static const float FRONT_FACING_VECTOR[3] = {0,0,1};

static const int FACE_TO_NORMAL[6][3] =
{
	-1,0,0,
	1,0,0,
	0,0,-1,
	0,0,1,
	0,1,0,
	0,-1,0
};

static const unsigned ROTATION_TO_ORIENTATION[6][6] = 
{
	0,1,2,3,4,5,
	1,0,2,3,4,5,
	
	2,3,1,0,4,5,
	3,2,0,1,4,5,

	4,5,2,3,1,0,
	5,4,2,3,0,1
};

//---------------
//rotation matrices following the right hand rule
//these rotations are all by positive 90 degrees
//---------------
static const int MATRIX_R_IDENTITY[9] =
{
	1,0,0,
	0,1,0,
	0,0,1
};

static const int MATRIX_R_90_X_TO_Y[9] =
{
	0,-1,0,
	1,0,0,
	0,0,1
};
static const int MATRIX_R_90_Y_TO_X[9] =
{
	0,1,0,
	-1,0,0,
	0,0,1
};

static const int MATRIX_R_90_Y_TO_Z[9] =
{
	1,0,0,
	0,0,-1,
	0,1,0
};

static const int MATRIX_R_90_Z_TO_Y[9] =
{
	1,0,0,
	0,0,1,
	0,-1,0
};

static const int MATRIX_R_90_X_TO_Z[9] =
{
	0,0,1,
	0,1,0,
	-1,0,0
};
static const int MATRIX_R_90_Z_TO_X[9] =
{
	0,0,-1,
	0,1,0,
	1,0,0
};

static const int * DIRECTION_ENUM_TO_R_MATRIX[7]=
{
	MATRIX_R_IDENTITY,
	NULL,

	MATRIX_R_90_Z_TO_X,
	MATRIX_R_90_X_TO_Z,
	
	
	MATRIX_R_90_Z_TO_Y,
	MATRIX_R_90_Y_TO_Z,

	NULL
};

inline QuVector3<int> faceToNormal(unsigned aFace)
{
	return QuVector3<int>(FACE_TO_NORMAL[aFace][0],FACE_TO_NORMAL[aFace][1],FACE_TO_NORMAL[aFace][2]);
}
inline unsigned normalToFace(QuVector3<int> aNorm)
{
	//six case checks
	//note this may still return if normal is invalid
	if(aNorm.x == 1)
		return 1;
	if(aNorm.x == -1)
		return 0;
	if(aNorm.y == 1)
		return 4;
	if(aNorm.y == -1)
		return 5;
	if(aNorm.z == 1)
		return 3;
	if(aNorm.z == -1)
		return 2;
	quAssert(false);
	return -1;
}