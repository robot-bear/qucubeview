#pragma once
#include "QuGameManager.h"
#include "QuGlobals.h"
#include "QuCubeMenu.h"

class QuCubeMenuExampleManager : public QuBaseManager
{
public:
	QuCubeMenuExampleManager()
	{
	}
	virtual void initialize()
	{
		QuStupidPointer<QuCubeMenuManager> menu( new QuCubeMenuManager());
		//TODO set it up

		G_GAME_GLOBAL.setManager(menu.safeCast<QuBaseManager>());
	}
};